package com.gridnine.testing.factories;

import com.gridnine.testing.models.Flight;
import com.gridnine.testing.models.Segment;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Predicate;

public class RuleBuilder {

    public static Predicate<Flight> createFlightDepartingInPastRule() {
        return flight -> flight.getSegments()
                .stream()
                .noneMatch(segment -> segment.getDepartureDate().compareTo(LocalDateTime.now()) < 0);
    }

    public static Predicate<Flight> createFlightDepartingBeforeItArrivesRule() {
        return flight -> flight.getSegments()
                .stream()
                .noneMatch(segment -> segment.getArrivalDate().compareTo(segment.getDepartureDate()) < 0);
    }

    public static Predicate<Flight> createFlightMore2HoursGroundTimeRule() {
        return flight -> {
            List<Segment> segments = flight.getSegments();
            int millis = 0;
            for (int i = 0; i < segments.size() - 1; i++) {
                millis += ChronoUnit.MILLIS
                        .between(segments.get(i).getArrivalDate(), segments.get(i + 1).getDepartureDate());
            }
            return millis < 7200000;
        };
    }
}
