package com.gridnine.testing;

import com.gridnine.testing.factories.FlightBuilder;
import com.gridnine.testing.factories.RuleBuilder;
import com.gridnine.testing.models.Flight;
import com.gridnine.testing.services.FlightsFilterService;
import com.gridnine.testing.services.FlightsFilterServiceImpl;

import java.util.List;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        List<Flight> flights = FlightBuilder.createFlights();

        Predicate<Flight> flightDepartingInPastRule = RuleBuilder.createFlightDepartingInPastRule();
        Predicate<Flight> flightDepartingBeforeItArrivesRule = RuleBuilder.createFlightDepartingBeforeItArrivesRule();
        Predicate<Flight> flightMore2HoursGroundTimeRule = RuleBuilder.createFlightMore2HoursGroundTimeRule();

        FlightsFilterService flightsFilterService = new FlightsFilterServiceImpl();
        flightsFilterService.filter(flights, List.of(flightDepartingInPastRule)).forEach(System.out::println);
        System.out.println();
        flightsFilterService
                .filter(flights, List.of(flightDepartingBeforeItArrivesRule)).forEach(System.out::println);
        System.out.println();
        flightsFilterService.filter(flights, List.of(flightMore2HoursGroundTimeRule)).forEach(System.out::println);
    }
}
