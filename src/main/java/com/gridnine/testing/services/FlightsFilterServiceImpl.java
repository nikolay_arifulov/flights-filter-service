package com.gridnine.testing.services;

import com.gridnine.testing.models.Flight;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FlightsFilterServiceImpl implements FlightsFilterService {

    @Override
    public List<Flight> filter(List<Flight> flights, List<Predicate<Flight>> rules) {
        for (Predicate<Flight> rule : rules) {
            flights = flights.stream().filter(rule).collect(Collectors.toList());
        }
        return flights;
    }
}
