package com.gridnine.testing.services;

import com.gridnine.testing.models.Flight;

import java.util.List;
import java.util.function.Predicate;

public interface FlightsFilterService {

    List<Flight> filter(List<Flight> flights, List<Predicate<Flight>> rules);
}
